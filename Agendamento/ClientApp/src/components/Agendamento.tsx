import React, {useState, ChangeEvent, useEffect} from 'react';
import apiAgenda, {EstruturaEvento} from '../services/agenda';
import MensagemAlerta from './GenericComponents/MensagemAlerta';
import DatePickerAgendamento from './GenericComponents/DatePickerAgendamento';
import {SelectOption} from '../helpers/genericInterfaces';
import Select, {ActionMeta, ValueType, OptionTypeBase} from 'react-select';
import {ok} from 'assert';

const Agendamento = () => {
  const [responsavel, setResponsavel] = useState('');
  const [dataInicio, setDataInicio] = useState<Date | null>(null);
  const [dataTermino, setDataTermino] = useState<Date | null>(null);
  const [salas, setSalas] = useState<Array<SelectOption>>();
  const [salaSelected, setSalaSelected] = useState<SelectOption>();
  const [errosAgendamento, setErrosAgendamento] = useState<Array<string>>([]);
  const loopController = false;

  const handleResponsavel = (event: ChangeEvent<HTMLInputElement>) => {
    setResponsavel(event.target.value);
    setErrosAgendamento([]);
  };

  const handleDataInicio = (newDate: Date) => {
    setDataInicio(newDate);
    setErrosAgendamento([]);
  };

  const handleDataTermino = (newDate: Date) => {
    setDataTermino(newDate);
    setErrosAgendamento([]);
  };

  useEffect(() => {
    async function obterSalas() {
      const {ok, data} = await apiAgenda.getSalas();
      if (ok && data) {
        setSalas(data);
      }
    }
    obterSalas();
  }, [loopController]);

  const selectBase = {
    placeholder: 'Selecione uma opção',
    isMultiple: false,
    isSearchable: true,
    isClearable: true,
    isDisabled: false,
    value: null,
    getOptionLabel: (option: SelectOption) => {
      return option.text;
    },
    getOptionValue: (option: SelectOption) => option.value,
  };

  const selectSalas = {
    ...selectBase,
    options: salas,
    onChange: (
      option: ValueType<OptionTypeBase>,
      action: ActionMeta<OptionTypeBase>
    ) => {
      onSelectChangeHandler(option, action, setSalaSelected);
      setErrosAgendamento([]);
    },
    value: salaSelected || null,
  };

  const onSelectChangeHandler = (
    option: ValueType<OptionTypeBase>,
    action: ActionMeta<OptionTypeBase>,
    setFunction: React.SetStateAction<any>
  ) => {
    if (action.action === 'select-option') {
      setFunction(option);
    } else if (action.action === 'clear') {
      setFunction(undefined);
    }
  };

  const validarDadosAgendamento = () => {
    let valido = true;
    if (responsavel == '') {
      setErrosAgendamento((prevState) => [
        ...prevState,
        'O campo Responsável é obrigatório.',
      ]);
      valido = false;
    }
    if (dataInicio == null) {
      setErrosAgendamento((prevState) => [
        ...prevState,
        'O campo Data Inicio é obrigatório.',
      ]);
    }
    if (dataTermino == null) {
      setErrosAgendamento((prevState) => [
        ...prevState,
        'O campo Data Termino é obrigatório.',
      ]);
    }
    if (salaSelected == null) {
      setErrosAgendamento((prevState) => [
        ...prevState,
        'O campo Sala é obrigatório.',
      ]);
    }

    return valido;
  };

  const handleAgendar = async () => {
    setErrosAgendamento([]);
    if (validarDadosAgendamento()) {
      if (salaSelected != null) {
        const {ok, data} = await apiAgenda.agendarSala({
          nomeResponsavel: responsavel,
          dataInicio: dataInicio,
          dataTermino: dataTermino,
          salaId: Number(salaSelected.value),
        });
        if (ok && data) {
          if (data.sucesso) {
            alert(data.retorno);
            
          } else {
            setErrosAgendamento((prevState) => [...prevState, data.retorno]);
          }
        }
      }
    }
  };

  return (
    <>
      <div className="block">
        <div className="block-header">
          <div className="block-title">
            <h4>Agendamento</h4>
          </div>
        </div>
        <div className="block-content">
          {errosAgendamento.length > 0 && (
            <MensagemAlerta mensagem={errosAgendamento} tipo="erro" show />
          )}
          <form>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Responsável</label>
              <input
                type="text"
                className="form-control"
                id="txtResponsavel"
                onChange={handleResponsavel}
              />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Data Início</label>
              <DatePickerAgendamento
                minDate={new Date()}
                selectedDate={dataInicio}
                onChange={handleDataInicio}
              />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Data Término</label>
              <DatePickerAgendamento
                minDate={new Date()}
                selectedDate={dataTermino}
                onChange={handleDataTermino}
              />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Sala</label>
              <Select {...selectSalas} />
            </div>
          </form>
        </div>
        <div className="block-buttons">
          <button
            type="button"
            onClick={handleAgendar}
            className="btn btn-primary"
          >
            <span>Agendar</span>
          </button>
        </div>
      </div>
    </>
  );
};

export default Agendamento;
