import React, { useState } from 'react';
import DatePicker from 'react-datepicker';

import ptbr from 'date-fns/locale/pt-BR';

const meses = [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maio',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro',
];
const dias = ['Dom', '2ª', '3ª', '4ª', '5ª', '6ª', 'Sáb'];

const locale = {
  ...ptbr,
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  localize: { ...ptbr.localize!, month: (n: number) => meses[n], day: (n: number) => dias[n] },
};

export interface DatePickerAgendamentoProps {
  dateOnly?: boolean;
  timeOnly?: boolean;
  minDate?: Date | undefined;
  maxDate?: Date | undefined;
  minTime?: Date | undefined;
  maxTime?: Date | undefined;
  selectedDate?: Date | null;
  onChange: (date: Date) => void;
}

const DatePickerAgendamento = (props: DatePickerAgendamentoProps) => {
  const { dateOnly, timeOnly, minDate, maxDate, minTime, maxTime, selectedDate, onChange } = props;

  const hadleDateChanged = (newDate: Date) => {
    onChange(newDate);
  };

  return (
    <>
      {dateOnly && (
        <DatePicker
          className="form-control"
          selected={selectedDate}
          onChange={hadleDateChanged}
          placeholderText="dd/mm/aaaa"
          locale={locale}
          dateFormat="dd/MM/yyyy"
          minDate={minDate}
          maxDate={maxDate}
        />
      )}
      {timeOnly && (
        <DatePicker
          className="form-control"
          selected={selectedDate}
          onChange={hadleDateChanged}
          showTimeSelect
          showTimeSelectOnly
          timeIntervals={5}
          timeCaption="Hora"
          placeholderText="hh:mm"
          locale={ptbr}
          dateFormat="HH:mm"
          timeFormat="HH:mm"
          minDate={minDate}
          maxDate={maxDate}
          minTime={minTime}
          maxTime={maxTime}
        />
      )}
      {!dateOnly && !timeOnly && (
        <DatePicker
          className="form-control"
          selected={selectedDate}
          onChange={hadleDateChanged}
          placeholderText="dd/mm/aaaa hh:mm"
          locale={locale}
          showTimeSelect
          timeIntervals={5}
          timeCaption="Hora"
          dateFormat="dd/MM/yyyy HH:mm"
          minDate={minDate}
          maxDate={maxDate}
          minTime={minTime}
          maxTime={maxTime}
        />
      )}
    </>
  );
};

export default DatePickerAgendamento;
