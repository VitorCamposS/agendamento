import React from 'react';

type MensagemKey = 'sucesso' | 'alerta' | 'erro';

interface Props {
  tipo: MensagemKey;
  show?: boolean;
  unica?: boolean;
  mensagem: string[];
}
const MensagemAlerta = (props: Props) => {
  const { tipo, show, unica, mensagem } = props;

  let retorno = null;

  if (unica) {
    retorno = (
      <p
        className={`mensagem-${tipo}`}
        style={{
          display: show ? 'block' : 'none',
        }}
      >
        {mensagem[0]}
      </p>
    );
  } else {
    retorno = (
      <div
        className={`mensagem-${tipo}`}
        style={{
          display: show ? 'block' : 'none',
        }}
      >
        <ul>
          {mensagem.map(m => (
            <li>{m}</li>
          ))}
        </ul>
      </div>
    );
  }

  return retorno;
};

export default MensagemAlerta;