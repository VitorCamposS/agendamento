import api from "./apiLocal";
import { SelectOption } from "../helpers/genericInterfaces";

export interface EstruturaAgenda {
    sucesso: boolean;
    retorno: string;
}

export interface Sala {
    id: number;
    nome: string;
}

 export interface EstruturaEvento {
     nomeResponsavel: string;
     dataInicio: Date | null;
     dataTermino: Date | null;
     salaId: number;
 }

const getSalas = () => {
    return api.get<Array<SelectOption>>('/preencher-dropdown-salas');
}

const agendarSala = (estruturaEvento: EstruturaEvento) => {
    return api.post<EstruturaAgenda>('/agendar', estruturaEvento);
};

const apiAgenda = {
    getSalas,
    agendarSala
};

export default apiAgenda;