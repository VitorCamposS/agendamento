﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Agendamento.Entidades
{
    [Serializable]
    [DataContract]
    public class EstruturaErro
    {
        [DataMember]
        public int IndicadorErro { get; set; }

        [DataMember]
        public string CodigoErro { get; set; }

        [DataMember]
        public string DescricaoErro { get; set; }
    }
}
