﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Agendamento.Entidades
{
    [Serializable]
    [DataContract]
    public class EstruturaEvento
    {
        [DataMember]
        public string Responsavel { get; set; }
        [DataMember]
        public DateTime DataInicio { get; set; }
        [DataMember]
        public DateTime DataTermino { get; set; }
        [DataMember]
        public string Sala { get; set; }
    }
}
