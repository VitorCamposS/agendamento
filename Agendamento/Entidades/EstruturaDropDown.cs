﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Agendamento.Entidades
{
    [Serializable]
    [DataContract]
    public class EstruturaDropDown : EstruturaErro
    {
        [DataMember]
        public string Text { get; set; }
        [DataMember]
        public object Value { get; set; }
        [DataMember]
        public object OrderField { get; set; }
    }
}
