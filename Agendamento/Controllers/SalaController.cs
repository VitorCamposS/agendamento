﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agendamento.Data;
using Agendamento.Entidades;
using Agendamento.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Agendamento.Controllers
{
    [ApiController]
    public class SalaController : ControllerBase
    {        
        [Route("/preencher-dropdown-salas")]
        public async Task<object> PreencherDropdownSalas()
        {
            AgendamentoContext _context = new AgendamentoContext();
            List<Sala> listaSalas = new List<Sala>();
            List<EstruturaDropDown> estruturaDropDowns = new List<EstruturaDropDown>();
            try
            {
                listaSalas = await _context.Salas.ToListAsync();
                foreach(var sala in listaSalas)
                {
                    estruturaDropDowns.Add(new EstruturaDropDown
                    {
                        Text = sala.Nome,
                        Value = sala.Id,
                        OrderField = sala.Nome
                    });
                }
                return estruturaDropDowns;
            }
            catch (Exception ex)
            {
                return "Ocorreu um erro ao buscar as salas." + ex.Message; 
            }
        }
    }
}
