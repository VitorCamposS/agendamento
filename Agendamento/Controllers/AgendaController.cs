﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agendamento.Data;
using Agendamento.Entidades;
using Agendamento.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Agendamento.Controllers
{
    [ApiController]
    public class AgendaController : ControllerBase
    {
        [Route("/agendar")]
        [HttpPost]
        public async Task<object> AgendarSala(Evento evento)
        {
            AgendamentoContext _context = new AgendamentoContext();
            string retorno = "";
            bool sucesso = true;
            try
            {
                
                if(evento.DataInicio < evento.DataTermino)
                {
                     _context.Eventos.Add(evento);
                    await _context.SaveChangesAsync();
                    retorno = "Evento cadastrado com sucesso.";
                    return new { sucesso, retorno };

                } else
                {
                    retorno = "A Data de Término do Evento não pode ser anterior que a Data Inicial.";
                    sucesso = false;
                    return new { sucesso, retorno };
                }     
            }
            catch (Exception ex)
            {
                sucesso = false;
                retorno = ex.Message;
                return new { sucesso, retorno};
            }
            
        }
    }
}