﻿using Agendamento.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agendamento.Data
{
    public class AgendamentoContext : DbContext
    {
        public DbSet<Evento> Eventos { get; set; }
        public DbSet<Sala> Salas { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=agendamento;Integrated Security=True");
        }
    }
}
